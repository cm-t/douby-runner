function unModal(modale) {
    const modal = document.getElementById(modale);
    modal.style.display = "none";
}

function modal(modale) {
    const modal = document.getElementById(modale);
    modal.style.display = "block";
}

function suivant(step) {
    const astuce = document.getElementById(`welcome-${step}`);
    const astuceNext = document.getElementById(`welcome-${step + 1}`);
    astuce.classList.add("hidden");
    astuceNext.classList.remove("hidden");
}

function lancer() {
    if (document.getElementById("contact").value == '') {
        if (!confirm("Tu n'as pas saisis tout les champs.\nTu peux continuer comme cela, mais tu participeras pas au jeu concours.\nContinuer quand même ?")) {
            return false;
        }
        document.getElementById("nick").value = 'DoubyLover';
        document.getElementById("contact").value = '@DoubyLover';
    }
    if (document.getElementById("nick").value == '') {
        document.getElementById("nick").value = 'DoubyShadow';
    }
    unModal('modal-welcome');
    reStart();
}

function die() {
    const ending = Math.floor(Math.random() * 4) + 1;
    Array.from(document.getElementsByClassName("ending")).forEach(e => {
        e.classList.add("hidden");
    });

    const theEnd = document.getElementById(`end-${ending}`);
    theEnd.classList.remove("hidden");
    modal('modal-dead');

    scoreAndScoreboard();
}

function respaune() {
    unModal('modal-dead');
    reStart();
}

function credit() {
    modal('modal-credit');
}

function scoreboard() {
    var loadscoreboard = document.getElementById('loadScore');
    loadscoreboard.classList.remove("hidden");
    var closescoreboard = document.getElementById('btn-fermer-score');
    closescoreboard.classList.add("hidden");
    var scoreboard = document.getElementById('scoreboard');
    scoreboard.classList.add("hidden");
    modal('modal-score');
    getScoreBoard();
}

function scoreAndScoreboard() {
    const loadscoreboard = document.getElementById('loadScore');
    loadscoreboard.classList.remove("hidden");
    const closescoreboard = document.getElementById('btn-fermer-score');
    closescoreboard.classList.add("hidden");
    const scoreboard = document.getElementById('scoreboard');
    scoreboard.classList.add("hidden");
    modal('modal-score');
    setScore();
}

function fermer(laModale) {
    unModal('modal-' + laModale);
}

function setScore() {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", 'score.php', true);

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            console.log(xhr.response);
            if (xhr.response == '1') {
                getScoreBoard();
            } else {
                alert("Désolé, une erreur est survenue lors de la sauvegarde du score");
                document.location.reload();
            }

        }
    }

    const vals = ['nick',  'contact', 'score', 'when', 'token'];
    const payload = vals.map((e) => {
        return `${e}=${document.getElementById(e).value}`;
    }).join('&');

    xhr.send(payload);
}

function getScoreBoard() {
    var url = 'scoreboard.php?contact=' + document.getElementById("contact").value;
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
            const scoreboard = document.getElementById('scoreboard');
            scoreboard.innerHTML = xhr.response;
            const loadscoreboard = document.getElementById('loadScore');
            loadscoreboard.classList.add("hidden");
            scoreboard.classList.remove("hidden");
            const closescoreboard = document.getElementById('btn-fermer-score');
            closescoreboard.classList.remove("hidden");
        }
    }

    xhr.open('GET', url, true);
    xhr.send('');
}