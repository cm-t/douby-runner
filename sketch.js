let score = 0;
let scroll = 10;
let scrollBg = 0;
let trains = [];
let trainsMedia = new Map();
let bg = {};
let douby = null;
let failSounds = [];
let jumper = new Map();
// jump sounds
let jump = {};
let scoreBg = null;
let colision = null;
let restart = true;
let baseUrl = './media';
let pauseGame = true;
let music = null;

/**
 * Return possible variant if underground or sol obstacle.
 * Selection is random, probability somewhat hardcoded ;)
 * @param {string} ground either 'sol' or 'underground'
 */
const varianteSelecter = (ground) => {
    const variantes = {
        underground: Array.from('ABCDPACDACDBB'),
        sol: Array.from('ABCDEPACDACDD'),
    };

    const v = variantes[ground];
    const variante = v[getRandomInt(0, v.length)];
    return variante;
}

/** Return a random integer between `start` and `end`
 * The maximum is exclusive and the minimum is inclusive
 * @param {integer} start
 * @param {integer} end
 * @returns {integer}
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
 */
const getRandomInt = (start, end) => {
    return Math.floor(Math.random() * (end - start) + start);
};


function preload() {
    music = loadSound(getMediaName('ground', 'ambiance', 'all', 'all', 'all', 'sound'));
    jump = {
        sol: loadSound(getMediaName('douby', 'jump', 'sol', 'A', 1, 'sound')),
        underground: loadSound(getMediaName('douby', 'jump', 'underground', 'A', 1, 'sound')),
        score100: loadSound(getMediaName('douby', 'score', '100', 'A', 1, 'sound')),
        score500: loadSound(getMediaName('douby', 'score', '500', 'A', 1, 'sound')),
    }

    colision = {
        default: loadSound(getMediaName('obstacles', 'colision', 'all', 'all', 'all', 'sound')),
        bonus: loadSound(getMediaName('obstacles', 'colision', 'all', 'B', 'all', 'sound')),
        to_sol: loadSound(getMediaName('obstacles', 'colision', 'sol', 'P', 1, 'sound')),
        to_underground: loadSound(getMediaName('obstacles', 'colision', 'underground', 'P', 1, 'sound')),
    };

    bg.sol = loadImage(getMediaName('ground', 'bg', 'sol', 'A', 1, 'img'));
    bg.underground = loadImage(getMediaName('ground', 'bg', 'underground', 'A', 1, 'img'));

    scoreBg = loadImage(getMediaName('ground', 'score', 'all', 'all', 'all', 'img'));

    for (let i = 1; i <= 3; i++) {
        jumper.set(getMediaId('douby', 'jump', 'sol', 'A', i, 'img'), loadImage(getMediaName('douby', 'jump', 'sol', 'A', i, 'img')));
        jumper.set(getMediaId('douby', 'jump', 'underground', 'A', i, 'img'), loadImage(getMediaName('douby', 'jump', 'underground', 'A', i, 'img')));
        jumper.set(getMediaId('douby', 'run', 'sol', 'A', i, 'img'), loadImage(getMediaName('douby', 'run', 'sol', 'A', i, 'img')));
        jumper.set(getMediaId('douby', 'run', 'underground', 'A', i, 'img'), loadImage(getMediaName('douby', 'run', 'underground', 'A', i, 'img')));
    }

    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'A', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'A', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'B', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'B', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'C', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'C', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'D', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'D', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'E', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'E', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'sol', 'P', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'sol', 'P', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'underground', 'A', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'underground', 'A', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'underground', 'B', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'underground', 'B', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'underground', 'C', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'underground', 'C', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'underground', 'D', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'underground', 'D', 1, 'img')));
    trainsMedia.set(getMediaId('obstacles', 'arrive', 'underground', 'P', 1, 'img'), loadImage(getMediaName('obstacles', 'arrive', 'underground', 'P', 1, 'img')));
}

function setup() {
    createCanvas(windowWidth * 0.98, windowHeight * 0.98);
    douby = new Douby();
    modal('modal-welcome');
}

function keyPressed() {
    if (key === ' ') {
        douby.jump();
        return false;
    }
}

function mouseClicked() {
    douby.jump();
    return false;
}

function touched() {
    douby.jump();
    return false;
}


function reStart() {
    pauseGame = false;
    restart = true;
    jumpstart();
}

function jumpstart() {
    restart = false;
    score = 0;
    scollBg = 0;
    scroll = 10;
    trains = [];
    music.play();
    music.setLoop(true);
    music.setVolume(0.2);
    loop();
    douby = new Douby();
    douby.jump();
    return false;
}

function draw() {
    const ground = douby.ground === 'sol' ? bg.sol : bg.underground;
    image(ground, -scrollBg, 0, width, height);
    image(ground, -scrollBg + width, 0, width, height);

    if (pauseGame) {
        return true;
    }

    if (scrollBg > width) {
        scrollBg = 0;
    }

    // plz don't keep old obstacles memory is pristine.
    trains = trains.filter(t => t.x != 0)

    // Adding obstacles Yay fun4all
    if (random(1) < 0.75 && frameCount % 50 === 0) {
        const variante = varianteSelecter(douby.ground)
        const obstacle = new Train(variante, douby.ground);
        trains.push(obstacle);
    }

    if (score !== 0) {
        if (score % 100 === 0 && !(score % 500 === 0)) {
            jump.score100.play();
        } else if (score % 500 === 0 && score !== 0) {
            jump.score500.play();
        }
    }

    if (frameCount % 5 == 0) {
        score++;
    }

    image(scoreBg, 0, 0, 240, 96);
    textSize(28);
    textFont('Comic Sans MS');
    text(` ${score}`, 114, 57);

    for (const t of trains) {
        t.move();
        t.show();

        if (douby.collide(t)) {
            if (t.variante === 'P') { // either we go up or down
                const [soundAsset, ground] = (douby.ground === 'sol'
                    ? [colision.to_underground, 'underground']
                    : [colision.to_sol, 'sol']
                );
                soundAsset.play();
                douby.ground = ground;
            } else if (t.variante === 'B') { // superBonus
                colision.bonus.play();
                score += 42;
            } else { // or we just end
                leFin();
            }
            t.r = 0;
            t.x = 0;
        }
    }
    douby.show()
    douby.move()

    scroll += 0.005;
    scrollBg += scroll / 2;
}

function leFin() {
    noLoop()
    music.stop();
    //let sound = random(failSounds)
    //sound.play();
    colision.default.play();

    document.getElementById("score").value = score;
    document.getElementById("when").value = Date.now();
    document.getElementById("token").value = 0;
    fill(0);
    die();
    restart = true;
    pauseGame = true;
}

function getMediaId(objectName, action, ground, variante, step, type) {
    const res = `${objectName}_${type}_${action}_${ground}_${variante}_${step}`;
    return res;
}

function getMediaName(objectName, action, ground, variante, step, type) {
    let types = {
        img: "png",
        sound: "mp3"
    };

    const res = `${baseUrl}/${objectName}/${type}/${action}_${ground}_${variante}_${step}.${types[type]}`;
    return res;
}

class Douby {
    constructor() {
        this.solCorrection = 10;
        this.r = 100;
        this.x = 105;
        this.y = height - this.r;
        this.prevY = 0;
        this.vy = 0;
        this.gravity = 2;
        this.maxGravity = 2;
        this.previousStep = 0;
        this.step = 1;
        this.maxStep = 3;
        this.up = true;
        this.ground = 'sol';
        this.variante = 'A';
        this.action = 'run';
    }

    move() {
        this.y += this.vy;
        this.vy += this.gravity;
        this.y = constrain(this.y, 0, height - this.r - this.solCorrection);

        if (this.y === (height - this.r - this.solCorrection)) { // si au sol
            this.action = 'run';
            this.step = (1 + (score % this.maxStep));
        } else {
            //console.log("pY" + this.prevY + "  Y: " + this.y + "     XY: " + this.vy);
            if (this.step === 1 && this.vy > -16 && this.vy < 12) {
                this.step = 2;
            }
            if (this.step === 2 && this.vy > 2) {
                this.step = 3;
            }
        }
    }

    jump() {
        if (this.action === 'jump') {
            return true;
        }

        if (this.y === height - this.r - this.solCorrection) {
            this.vy = -32;
            const asset = Math.random() > 0.5 ? jump.sol : jump.underground;
            asset.play();
            this.gravity = this.maxGravity;
        }

        this.action = 'jump';
        this.step = 1;
    }

    collide(other) {
        const hitX = (this.x + this.r > other.x &&
            this.x < other.x + other.r);
        const hitY = this.y + this.r > other.y;
        return (hitX && hitY);
    }

    show() {
        let ajustement = 1;
        if (this.ground === 'underground' && this.action === 'run') {
            ajustement = 1.6;
        }
        const asset = getMediaId('douby', this.action, this.ground, this.variante, this.step, 'img');
        image(jumper.get(asset), this.x - this.r * ajustement, this.y - this.r * ajustement * (ajustement != 1 ? 0.8 * ajustement : 1), ajustement * 2 * this.r, ajustement * 2 * this.r);
    }
}

class Train {
    /**
     * Represent an obtacle
     * @param {string} variante Either 'sol' or 'underground'
     * @param {string} ground current variation
     */
    constructor(variante, ground) {
        // Constants after instanciation
        this.ground = ground;
        this.variante = variante
        this.solCorrection = 5;
        this.action = 'arrive';

        this.r = 85;
        this.x = width;
        this.y = height - this.r - this.solCorrection;

        this.maxStep = 1;
        this.step = 1;

        if (this.ground === 'sol' && (this.variante === 'D' || this.variante === 'E')) {
            this.y = 45;
        } else if (this.ground === 'underground') {
            this.y = height - this.r - (3 * this.solCorrection);
        }
    }

    move() {
        this.x -= scroll;
        if (this.ground === 'sol' && (this.variante === 'D' || this.variante === 'E')) { // badbird !
            this.y = height - (1.4 * this.x / Math.log(this.x)) - (0.6 * this.r);
        }
    }

    show() {
        const id = getMediaId('obstacles', this.action, this.ground, this.variante, this.step, 'img');
        const assert = trainsMedia.get(id);
        image(assert, this.x, this.y - this.r, 2 * this.r, 2 * this.r);
    }
}
