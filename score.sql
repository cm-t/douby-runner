CREATE TABLE IF NOT EXISTS `douby_score` (
  `id` int(11) NOT NULL auto_increment,
  `nick` varchar(42) collate utf8_unicode_ci NOT NULL,
  `contact` varchar(254) collate utf8_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `when` varchar(254) collate utf8_unicode_ci NOT NULL,
  `token` varchar(254) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
